#! /usr/bin/env python3

import sys
import nltk
from nltk.corpus import brown
from nltk.tokenize import RegexpTokenizer
import string

def readAndTagData():
	#brownSent = brown.sents()
	ifile = open(sys.argv[1],"rt")
	ofile = open(sys.argv[2],"wt")
	
	brownSent = ifile.readlines()
	#homophonesList = ["They're","They 're","they're","they 're",'their',"Their","Your","your","You're","You 're","you're","you 're","too","Too","to","To","lose","loose","Loose","Lose","it's","it 's","It's","It 's","its","Its","It's","It 's"]
	homophonesList = ["they're","They're",'their',"Their"]
	
	for sen in brownSent:		
		#tokenizer = RegexpTokenizer("[a-zA-Z]+'[a-zA-Z]+|[a-zA-Z]+|(\d+,\d+)+|(\d+/\d+)+|\$(\d+\.\d+)+|\?+|\*+|-+|\.+|\,+|'+|\"+|\S+")
		#senSplit = tokenizer.tokenize(se)
		
		senSplit = sen.split()
		i=0
		for cword in senSplit:
			if cword in homophonesList:
				if cword=="their" or cword == "Their":
					word = "their"
				else:
					word = "they're"			
				taggedText = nltk.pos_tag(senSplit)				
				if len(taggedText) == 1:
					ofile.write(word+" ")
					ofile.write("ppword:BOS1"+" ")
					ofile.write("pptag:TAG1"+" ")
					ofile.write("pword:BOS2"+" ")
					ofile.write("ptag:TAG2"+" ")
					ofile.write("nword:EOS1"+" ")
					ofile.write("ntag:ETAG1"+" ")
					ofile.write("nnword:EOS2"+" ")
					ofile.write("nntag:ETAG2"+"\n")
					
				elif i== len(taggedText)-2:
					ofile.write(word+" ")
					ofile.write("ppword:"+taggedText[i-2][0]+" ")
					ofile.write("pptag:"+taggedText[i-2][1]+" ")
					ofile.write("pword:"+taggedText[i-1][0]+" ")
					ofile.write("ptag:"+taggedText[i-1][1]+" ")
					ofile.write("nword:"+taggedText[i+1][0]+" ")
					ofile.write("ntag:"+taggedText[i+1][1]+" ")
					ofile.write("nnword:EOS2"+" ")
					ofile.write("nntag:ETAG2"+"\n")
					
				elif i== len(taggedText)-1:
					ofile.write(word+" ")
					ofile.write("ppword:"+taggedText[i-2][0]+" ")
					ofile.write("pptag:"+taggedText[i-2][1]+" ")
					ofile.write("pword:"+taggedText[i-1][0]+" ")
					ofile.write("ptag:"+taggedText[i-1][1]+" ")
					ofile.write("nword:EOS1"+" ")
					ofile.write("ntag:ETAG1"+" ")
					ofile.write("nnword:EOS2"+" ")
					ofile.write("nntag:ETAG2"+"\n")
					
				elif i==0:
					ofile.write(word+" ")
					ofile.write("ppword:BOS1"+" ")
					ofile.write("pptag:TAG1"+" ")
					ofile.write("pword:BOS2"+" ")
					ofile.write("ptag:TAG2"+" ")
					ofile.write("nword:"+taggedText[i+1][0]+" ")
					ofile.write("ntag:"+taggedText[i+1][1]+" ")
					ofile.write("nnword:"+taggedText[i+2][0]+" ")
					ofile.write("nntag:"+taggedText[i+2][1]+"\n")
					
				elif i==1:
					ofile.write(word+" ")
					ofile.write("ppword:BOS1"+" ")
					ofile.write("pptag:TAG1"+" ")
					ofile.write("pword:"+taggedText[i-1][0]+" ")
					ofile.write("ptag:"+taggedText[i-1][1]+" ")
					ofile.write("nword:"+taggedText[i+1][0]+" ")
					ofile.write("ntag:"+taggedText[i+1][1]+" ")
					ofile.write("nnword:"+taggedText[i+2][0]+" ")
					ofile.write("nntag:"+taggedText[i+2][1]+"\n")
				else:
					ofile.write(word+" ")
					ofile.write("ppword:"+taggedText[i-2][0]+" ")
					ofile.write("pptag:"+taggedText[i-2][1]+" ")
					ofile.write("pword:"+taggedText[i-1][0]+" ")
					ofile.write("ptag:"+taggedText[i-1][1]+" ")
					ofile.write("nword:"+taggedText[i+1][0]+" ")
					ofile.write("ntag:"+taggedText[i+1][1]+" ")
					ofile.write("nnword:"+taggedText[i+2][0]+" ")
					ofile.write("nntag:"+taggedText[i+2][1]+"\n")
				
			i =i+1
	ofile.close()


readAndTagData()
