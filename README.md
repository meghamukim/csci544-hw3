# README #

Approach for HW3( Homophone Disambiguation):

Classification using MegaM

1. I used the untagged version of Brown Corpus data from the NLTK to train my classifier. (http://www.nltk.org/)
2. The nltk.pos_tag() function was used to tag my data for POS Tagging.
3. After this the feature files were formed. Each label(lose vs loose) given in the HW description had a feature file associated with it, which resulted in five feature files.
4. Then these feature files were given as input to MegaM(http://www.umiacs.umd.edu/~hal/megam/) to create a model and then use the model for classification purpose of the test data.
5. After all the labels were classified, these were used to replace the predicted words in the error file.

So each of this has to be called 5 times for each label.

To form the feature files use the following command:
python3 hw3.py <SourceFile> <FeatureFileName>

To write the predicted labels in the error file use the following command:
python3 writepredicted.py <ErrorFile> <OutputFileName> 