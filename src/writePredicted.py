#!/usr/bin/env python3

import sys
from nltk.tokenize import RegexpTokenizer

def writepredictedData():
	f1 = open(sys.argv[1],"rt")
	f2 = open(sys.argv[2],"rt")
	ofile = open(sys.argv[3],"wt")
	
	#homophonesList = ["They're","they're",'their',"Their","Your","your","You're","you're","too","Too","to","To","lose","loose","Loose","Lose","it's","its","Its","It's"]
	homophonesList = ["too","Too","to","To"]
	j=0
	l1= f1.readlines()
	l2 = f2.readlines()
	for line in l1:		
		#tokenizer = RegexpTokenizer("[a-zA-Z]+'[a-zA-Z]+|[a-zA-Z]+|(\d+,\d+)+|(\d+/\d+)+|\$(\d+\.\d+)+|\?+|\*+|-+|\.+|\,+|'+|\"+|\S+")
		#senSplit = tokenizer.tokenize(l1)
		senSplit=line.split()
		for cword in senSplit:
			if cword in homophonesList:
				l = l2[j].split("\n")[0]
				if l=="0":
					if cword[0].isupper():
						ofile.write("To"+" ")
					else:
						ofile.write("to"+" ")
				else:
					if cword[0].isupper():
						ofile.write("Too"+" ")
					else:
						ofile.write("too"+" ")
				j +=1
			else:
				ofile.write(cword+" ")
		ofile.write("\n")
	f1.close()
	f2.close()
	ofile.close()

writepredictedData()
